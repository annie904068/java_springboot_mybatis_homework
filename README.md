1. Framework: Spring Boot、MyBatis

2. 主程式在src/main/java/com/tgl/mvc/WebApplication.java

3. 啟動執行 mvn spring-boot:run

4. 可使用 swagger UI 或是 Postman 測試

5. swagger UI: http://localhost:8080/swagger-ui.html 從try-it-out即可測試CRUD

6. Postman:

        (1) 新增: 使用Post，http://localhost:8080/rest/employee ，輸入新增的條件，成功會回傳id

        (2) 查詢: 使用Get，http://localhost:8080/rest/employee/ ，最後加上欲查詢的id，成功會回傳查詢到的內容

        (3) 修改: 使用Put，http://localhost:8080/rest/employee ，輸入修改的條件，成功會回傳修改過後的內容

        (4) 刪除: 使用Delete，http://localhost:8080/rest/employee/ ，最後加上欲刪除的id，成功會回傳刪除的id