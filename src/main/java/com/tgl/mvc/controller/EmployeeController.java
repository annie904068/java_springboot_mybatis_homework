package com.tgl.mvc.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tgl.mvc.exception.RecordNotFoundException;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.EmployeeService;

@RestController
@RequestMapping(value = "/rest/employee")
public class EmployeeController {
	@Autowired
	private EmployeeService employeeService;

	@PostMapping
	public ResponseEntity<Integer> insert(@RequestBody @Valid Employee employee) {
		employeeService.insert(employee);
		return new ResponseEntity<>(employee.getId(), HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Integer> delete(@PathVariable("id") @Min(1) int employeeId) {
		Employee result = employeeService.findById(employeeId);
		if (result == null) {
			throw new RecordNotFoundException(employeeId);
		}
		employeeService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}

	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {
		Employee result = employeeService.findById(employee.getId());
		if (result == null) {
			throw new RecordNotFoundException(employee.getId());
		}
		employeeService.update(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<Employee> findById(@PathVariable("id") @Min(1) int employeeId) {
		Employee employee = employeeService.findById(employeeId);
		if (employee == null) {
			throw new RecordNotFoundException(employeeId);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
}
