package com.tgl.mvc.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.ResourceUtils;

import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;

public class EmployeeDataInputToSql {
	private Employee employee;

	public List<Employee> importData() {
		String line = "";
		List<Employee> result = new ArrayList<>();
		// 將resource中的data讀進來
		try (BufferedReader file = new BufferedReader(
				new FileReader(ResourceUtils.getFile("classpath:employeedata.txt")))) {
			while ((line = file.readLine()) != null) {
				employee = new Employee();
				String[] word = line.split(" "); // 讀進一行資料後，以空格切割
				// 長度不為6的話，代表輸入的資料有缺少
				if (word.length != 6) {
					System.out.printf("input error: %s \n", line);
					continue;
				} else {
					employee.setHeight(Integer.valueOf(word[0]));
					employee.setWeight(Integer.valueOf(word[1]));
					employee.setEngName(word[2]);
					employee.setChName(word[3]);
					employee.setPhone(word[4]);
					employee.setEmail(word[5]);
					employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight()));
					result.add(employee);
				}
			}
			file.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return result;
	}
}
