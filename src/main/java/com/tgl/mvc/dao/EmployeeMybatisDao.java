package com.tgl.mvc.dao;

import com.tgl.mvc.model.Employee;

public interface EmployeeMybatisDao {
	Employee findById(int id);
	
	boolean insert(Employee user);
	
	boolean update(Employee user);
	
	boolean delete(int id);
	
	boolean truncateTable();
}
