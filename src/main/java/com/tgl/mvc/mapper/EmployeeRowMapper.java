package com.tgl.mvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

import com.tgl.mvc.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee emp = new Employee();
		emp.setId(rs.getInt("id"));
		emp.setChName(rs.getString("chName"));
		emp.setEngName(rs.getString("engName"));
		emp.setPhone(rs.getString("phone"));
		emp.setEmail(rs.getString("email"));
		emp.setHeight(rs.getDouble("height"));
		emp.setWeight(rs.getDouble("weight"));
		emp.setBmi(rs.getDouble("bmi"));
		return emp;
	}
}
