package com.tgl.mvc.model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Employee {
	private int id;

	@DecimalMax(value = "300.0", message = "height must be equal or less than 300")
	@DecimalMin(value = "100.0", message = "height must be equal or greater than 100")
	private double height;

	@DecimalMax(value = "300.0", message = "weight must be equal or less than 300")
	@DecimalMin(value = "30.0", message = "weight must be equal or greater than 30")
	private double weight;

	@Size(min = 4, max = 30, message = "Please provide a valid english name")
	private String engName;

	@Size(min = 2, max = 4, message = "Please provide a valid chinese name")
	private String chName;

	@NotEmpty(message = "Please provide a phone number")
	@Pattern(regexp = "^[0-9]{4}", message = "Please provide a 4-digits phone number")
	private String phone;

	@Email(message = "Email must be a valid email address")
	@Size(max = 60, message = "Email address character should less than 60")
	private String email;

	private double bmi;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getChName() {
		return chName;
	}

	public void setChName(String chName) {
		this.chName = chName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = bmi;
	}
}
