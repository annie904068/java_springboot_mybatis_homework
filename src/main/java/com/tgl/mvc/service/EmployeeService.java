package com.tgl.mvc.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.mvc.dao.EmployeeDataInputToSql;
import com.tgl.mvc.dao.EmployeeMybatisDao;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.DataUtil;

@Service
public class EmployeeService {
	@Autowired
	private EmployeeMybatisDao employeeMybatisDao;

	public boolean insert(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); //計算bmi
		return employeeMybatisDao.insert(employee);
	}

	public boolean delete(int employeeId) {
		return employeeMybatisDao.delete(employeeId);
	}

	public boolean update(Employee employee) {
		employee.setBmi(DataUtil.bmi(employee.getHeight(), employee.getWeight())); //計算bmi
		return employeeMybatisDao.update(employee);
	}

	public Employee findById(int employeeId) {
		Employee result = employeeMybatisDao.findById(employeeId); //提供id，回傳那筆資料
		if (result == null) {
			return null;
		}
		String chName = result.getChName();
		String maskedName = DataUtil.hideName(chName); //將中文名字做隱碼
		result.setChName(maskedName);
		return result;
	}

	@PostConstruct
	public boolean initTable() {
		employeeMybatisDao.truncateTable(); //清空資料表
		System.out.println("truncate success");
		
		EmployeeDataInputToSql empToSql = new EmployeeDataInputToSql();
		List<Employee> emp = empToSql.importData(); // 讀檔
		for (Employee element : emp) {
			insert(element); //塞入資料
		}
		System.out.println("employee file input success");
		return true;
	}
}
