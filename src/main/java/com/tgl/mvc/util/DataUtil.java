package com.tgl.mvc.util;

public final class DataUtil {
	private DataUtil() {}

	//中文名字隱碼
	public static String hideName(String chName) {
		int nameLen = chName.length(); //計算名字長度
		if(chName == "" || nameLen == 0) {
			return ""; //如果是空字串或是長度為0，則回傳空字串
		}
		StringBuilder newname = new StringBuilder(); //newname是名字隱碼後的結果
		newname.append(chName.substring(0, 1)); //先將第一個字append進去
		if(nameLen == 2) {
			newname.append("*"); //如果名字只有兩個字，則加入一個*
		}else {
			for(int i = 0; i < nameLen-2; i++) {
				newname.append("*"); //名字長度2者，扣除頭尾，中間的字都改為*
			}
			newname.append(chName.substring(nameLen-1, nameLen)); //將最後一個字加回去
		}
		return newname.toString();
	}
	
	//count bmi
	public static double bmi(double height, double weight) {
		return weight/((height/100)*(height/100));
	}
}
